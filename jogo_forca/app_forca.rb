require 'tty-spinner'

spinner = TTY::Spinner.new("[:spinner] Carregando ...", format: :spin_2)
spinner.auto_spin # Automatic animation with default interval
sleep(0.7) # Perform task
spinner.stop("Pronto!") # Stop animation
sleep(0.5)
system('cls')

novo_jogo = "s"

while novo_jogo == "s"
    system('cls')
    palavras = %w{cachorro gato morcego aranha rato formiga golfinho camelo serpente crocodilo galinha pato}
    palavra_sorteada = palavras.sample
    palavra_tamanho = palavra_sorteada.size
 
    puts "Bem vindo ao jogo da forca!" 
 
    puts "Descubra o animal que estou pensando... "
 
 
    palavra_parcial = []
    palavra_tamanho.times do 
     palavra_parcial << " _ "
    end
 
    puts palavra_parcial.join
 
    fim = false
 
    while fim == false
     puts "Escolha uma letra: "
     sua_letra = gets.chomp
 
     aux = 0
 
        palavra_sorteada.each_char do |letra_palavra|
            if palavra_parcial[aux] == ' _ '
                if letra_palavra == sua_letra
                    palavra_parcial[aux] = sua_letra
                end
            end
                   
         aux += 1
        end
 
        if palavra_parcial.join.count(" _ ") > 0
            fim = false
            puts palavra_parcial.join
        else
         fim = true
        end
    end
 
 puts ("Parabéns! O animal que eu estava pensando era #{palavra_parcial.join}!")
 puts "Deseja jogar novamente? (s/n)"
 novo_jogo = gets.chomp
end

puts "Obrigado por jogar!"
sleep(0.7)
system('cls')
