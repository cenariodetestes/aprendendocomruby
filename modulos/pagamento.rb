##Módulos: São parecidos com classe no sentido que armazenam métodos e constantes, mas se diferem pois não é possível instanciar (criar
# um objeto) e nem criar módulos que herdam desse módulo. o Módulo é mais utilizado para armazenar constantes e métodos num 
#lugar centralizado. 

module Pagamento
    def pagar(bandeira, numero, valor)
        "Pagando com o cartão #{bandeira} Número #{numero} o valor de R$#{valor},00"
    end
end
