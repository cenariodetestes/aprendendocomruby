#Teste#

=begin
Tudo que fica comentado entre begin e end configura
como comentário
=end

=begin
puts "Olá!"
puts "Digite o seu nome:"
name = gets.chomp
puts "Seu nome é: " + name

puts "Muito prazer em conhecer, " + name

puts "Agora me diga sua idade"
age = gets.chomp
puts "Beleza, descobri que sua idade é " + age + " anos. Um pouco velho você, né?"


# Para interpolar variaveis (chamar objetos dentro de uma string), eh necessario colocar os objetos com aspas duplas e
#   na string do comando puts a hashtag, chaves e a variavel dentro
nome = "Mari"
puts "Olá, tudo bem, #{nome}?"


#Coercao
puts "Digite sua idade:"
v1 = gets.chomp.to_i # aqui tem que add .to_i pra transformar a string em numero inteiro
v2 = v1 + 1 
puts "Sua idade ano que vem será: #{v2}"


# Operações aritméticas realizar utilizando irb
irb
1+2
5-3
4*3
3/4
5%2 #modulo
3**2
exit

# Operadores relacionais
a=2 
a += 2 # a = a + 2
puts a
a **= 3 # a = a**3
puts a


# Estruturas condicionais
puts "Digite um número:"
v1 = gets.chomp.to_i

#Condicional IF
if v1 > 10
    puts "O valor digitado é maior que 10"
elsif v1 == 10
    puts "O valor digitado é 10"
else
    puts "O valor digitado é menor que 10"
end

# Condicional UNLESS (a menos que)
unless v1 > 10
    puts "O número digitado é menor que 10"
else
    puts "O número digitado é maior que 10"
end

#Condicional CASE...WHEN 
puts "Escolha um número entre 0 e 5:"
v1 = gets.chomp.to_i

case v1 
when 0
    puts "Você escolheu o número 0"
when 1
    puts "Você escolheu o número 1"
when 2
    puts "Você escolheu o número 2"
when 3
    puts "Você escolheu o número 3"
when 4
    puts "Você escolheu o número 4"
when 5
    puts "Você escolheu o número 5"
else
    puts "Opção inválida: O número tem que ser entre 0 e 5"
end


#Operadores lógicos (&&, ||, !)
v1 = 34
v2 = 45
v3 = 2
v4 = 7

if (v1 < v2) && (v3 < v4) #  &&: AND
    puts "Condição atendida nos dois casos"
else 
    puts "Condição NÂO atendida nos dois casos"
end

if (v1 > v2) || (v3 < v4) # ||: OR
    puts "Pelo menos UMA condição atendida"
else
    puts "Condição NÂO atendida nos dois casos"
end

if !(v1 > v2) # !: NOT
    puts "Negação atendida"
else
    puts "Negação NÂO atendida"
end

# Estruturas de repetição
i = 1
while i <= 10 #Enquanto for verdadeiro
    puts "Leo -> #{i}"
    i += 1
end

until i <= 0 #Enquanto for falso
    puts "Sou maior que zero"
    i -= 1
end

puts "Finalizou!"

# Vetores
irb
v = []
v.push [45]
v.push [50]
v.push [2]
v[1]
v.delete(50)

# Hashes
irb
h = {"a" => 123, "jota" => "meia", "45" => 12}
h ["a"]
h ["jota"]
h ["45"]

# Símbolos: São strings estáticas que não alteram a posição na memória do Ruby
irb
{"a" => "sfgsfg"}
"a".class
{:a => "sfgsfg"}
:a.class
{a: "sfgsfg"}
"d".class
"d".to_sym


# Iterador "Each"
irb
a = [1,2,3,4,5,6,7]
a
a.each { |n| puts n } #esse |n| pode ser qualquer variavel dentro do ||, o que importa é repetir ele após a ação, nesse caso, puts.
a.each { |n| puts n*3}

# Operadores de intervalo
irb
[1,2,3,4,5,6,7]
(1...7).each { |n| puts n } 
(1..7).each { |n| puts n } 
#Quando vocÊ coloca dois pontinhos dentro de um intervalo, ele captura até o último número, enquanto que com tres pontinhos, ele 
# captura até o antepenúltimo número.

# Repetições fixas com o "Times"
irb
5.times { |x| puts "Leo"}
5.times { |x| puts "#{x}- Leo"}

####Criando uma classe
irb
class Conta
#####
#####
end
obj1 = Conta.new
obj1
obj1.class

###Definindo métodos
class Pessoa
    def gritar
        puts "Grrrrh!!!"
    end

    def agradecer (parametro = "Obrigado!") #aqui quando vc add um parametro, esse vai ser a resposta default
        puts parametro
    end
end

obj1 = Pessoa.new
obj1.gritar
obj1.agradecer
obj1.agradecer("Valeu demaixxx") #Se voce mudar o output do objeto, ele apresenta esse novo parametr e esquece o default

##Retorno de método
class Pessoa   
    def falar (texto = "blablablah")
        return "Olá, #{texto}" # o return aqui não é obrigatório, ele vai retornar o que estiver escrito após o nome do método
    end
end
puts obj1.falar

##Definindo atributos
class Pessoa
    @nome = nil #Isso aqui é chamado de variável de instância, pq cada instância(objeto) tem a sua variável
    @idade = nil

    def guardar_nome(nome)
        @nome = nome
    end

    def mostrar_nome
        @nome
    end

    def guardar_idade(idade)
        @idade = idade
    end

    def mostrar_idade
        @idade
    end
end

pessoa1 = Pessoa.new
pessoa1.guardar_nome("Leo Fidelis")
pessoa1.guardar_idade(25)

pessoa2 = Pessoa.new
pessoa2.guardar_nome("Mari Ishi")
pessoa2.guardar_idade(32)

puts pessoa1.mostrar_nome
puts pessoa1.mostrar_idade

puts pessoa2.mostrar_nome
puts pessoa2.mostrar_idade

## Se fosse em java, por convenção, no lugar de guardar_nome, se usaria set_nome, e no lugar de mostrar_nome seria get_nome
## A função é mesma, só por convenção, utiliza-se os (SET/GET)

#### No Ruby, seria bem mais facil, pois nao precisa utilizar sets and gets
###Perceba que no comando abaixo é bem mais simplificado
class Pessoa
    @nome = nil 
    @idade = nil

    def nome=(nome)
        @nome = nome
    end

    def nome
        @nome
    end

    def idade=(idade)
        @idade = idade
    end

    def idade
        @idade
    end
end

pessoa1 = Pessoa.new
pessoa1.nome = "Leo Fidelis"
pessoa1.idade= 25

pessoa2 = Pessoa.new
pessoa2.nome= "Mari Ishi"
pessoa2.idade= 32

puts pessoa1.nome
puts pessoa1.idade

puts pessoa2.nome
puts pessoa2.idade


##Ainda, existe o atalho, chamado de acessor, que facilita mais ainda a escrita no Ruby
##Ele subistitui todo o gets and sets
class Pessoa
  attr_accessor :nome
  attr_accessor :idade
end

pessoa1 = Pessoa.new
pessoa1.nome = "Leo Fidelis"
pessoa1.idade= 25

pessoa2 = Pessoa.new
pessoa2.nome= "Mari Ishi"
pessoa2.idade= 32

puts pessoa1.nome
puts pessoa1.idade

puts pessoa2.nome
puts pessoa2.idade
### Esta é a forma mais comum de definir os atributos no Ruby. Exite tbm o attr_writer só para o get e o attr_reader só para o set.
## O attr_accessor faz as duas coisas, set and get (reader and writer)


##Exercício
class Cachorro
    attr_accessor :nome
    attr_reader :raca

    def initialize (nome, raca)
     @nome = nome
     @raca = raca
    end

    def latir (latido = "au au!")
        puts latido
    end
end

dog1 = Cachorro.new("Rex", "Labrador")
dog2 = Cachorro.new("Bob", "PitBull")

dog1.latir
puts dog1.nome
puts dog1.raca

dog2.latir("Auuuuu late coração cachorro late coração")
puts dog2.nome
puts dog2.raca

class Papagaio
    attr_accessor :nome
    attr_accessor :idade

    def initialize (nome = "Louro", idade = 15)
        @nome = nome
        @idade = idade
    end

    def repetir_frase (repete = "Curupaco!")
        puts repete
    end
end

louro1 = Papagaio.new("José", 25)
louro2 = Papagaio.new("Mirna", 12)

louro1.repetir_frase
puts louro1.nome
puts louro1.idade

louro2.repetir_frase("Avemaria!")
puts louro2.nome
puts louro2.idade

### Jogo Adivinhar
class AdivinharNumero
    attr_reader :numero
    attr_reader :venceu

    def initialize 
        @numero = 5
        @venceu = false
    end

    def tentar_adivinhar(numero = 0)
        if numero == Random.rand(1..10)
            @venceu = true
            return "Você VENCEU!"
        elsif numero > @numero
            return "O número informado é muito alto..."
        else numero < @numero
            return "O número informado é muito baixo..." 
        end
    end
end

jogo = AdivinharNumero.new

until jogo.venceu
       puts "Digite um número:"
    numero = gets.to_i
    puts jogo.tentar_adivinhar(numero)
end


##Herança entre classes
# O símbolo < faz uma classe adquirir ("herdar") os métodos e atributos de uma classe criada anteriormente

class Pai
    attr_accessor :nome

    def falar (texto = "Alô!")
        texto
    end
end

class Filha < Pai
end

p = Pai.new
p.nome = "Leo"
puts p.nome
puts p.falar

puts ("-------------------------")

f = Filha.new
f.nome = "Maria"
puts f.nome
puts f.falar("Aloha!")


class Funcionario
    attr_accessor :nome, :cpf, :salario
end

class Gerente < Funcionario
    attr_accessor :senha, :tempo_empresa
end

f = Funcionario.new
f.nome = "Leonardo"
f.cpf = 65451987645
f.salario = 7000

puts f.nome
puts f.cpf
puts f.salario

puts ("----------------")

g = Gerente.new
g.nome = "Marlon"
g.cpf = 32198957869
g.salario = 12000
g.senha = 123456
g.tempo_empresa = 10


puts g.nome
puts g.cpf
puts g.salario
puts g.senha
puts g.tempo_empresa


##Overriding (Sobrescrita de método)
class Calculadora
    def somar (n1, n2)
        n1 + n2
    end
end

class CalculadoraFasion < Calculadora
    def somar (n1, n2) ##Aqui reabrimos o método para fazer alguma modificação, nesse caso a impressão
        puts "A soma é: #{n1 + n2}"
    end
end

c = Calculadora.new
puts c.somar(2,3)

cf = CalculadoraFasion.new
puts cf.somar(2,3)


### Método Super
## Serve para invocar um método já descrito na classe pai
class Conta
    attr_accessor :numero, :saldo 

    def initialize (numero, saldo = 0)
        @numero = numero
        @saldo = saldo
    end
end

class ContaEspecial < Conta
    attr_accessor :limite_especial

    def initialize (numero, saldo, limite_especial)
        super(numero, saldo) ###Realiza o initialize da classe pai (Conta)
        @limite_especial = limite_especial
    end
end

c = Conta.new("001", 100.00)
puts c.numero
puts c.saldo

ce = ContaEspecial.new("002", 200.00, 1000.00)
puts ce.numero
puts ce.saldo
puts ce.limite_especial


#Método de instância vs Método de classe, utilizando o "self"
# O "self" é um comando que chama a propria classe e nao é necessario instanciar um objeto
class Teste
    def ola #Método de Instância
        "Olá"
    end

    def self.hello #Método de classe
        "Hello!"
    end
end

obj = Teste.new #Aqui é necessário instanciar uma classe para depois
puts obj.ola    #solicitar o método

puts Teste.hello #Aqui voce não precisa criar um objeto (instanciar uma classe), já chama o método diretamente

##Constantes: são definidas por letras maiusculas, diferentes das variaveis que sao letras minusculas, a constante é feita
##para não alterar seu valor o tempo todo, como as variaveis 
#Classes tbm podem ter valores constantes
class Teste
    NOME_APP = "CRM"
    NOME_CLIENTE = "Fulano"
end

puts Teste::NOME_APP
puts Teste::NOME_CLIENTE

=end

###Duck typing: Um mesmo método em classes diferentes. Tbm é uma forma de polimorfismo
class Pato
    def quack!
        "Quack! Quack!"
    end
end

class PatoDoente
    def quack!
        "Queeeeck..."
    end
end

class Pessoa
    def apertar_o_pato(pato)
        pato.quack!
    end
end

p1 = Pato.new
p2 = PatoDoente.new

p = Pessoa.new

puts p.apertar_o_pato(p1)
puts p.apertar_o_pato(p2)


